  var errors = require('../lib/errors.js'),
      GPushMessenger = require('../lib/gcm.js'),
      Q = require('q'),
      Device = require('./media/device'),
      User = require('./user/user').UserModel,
      TCLocation = require('./user/locations').TCLocation,
      CheckLog = require('./user/locations').CheckLog,
      FeedBackAnswers = require('./user/locations').FeedBackAnswer,
      Message = require('./message'),
      _ = require('lodash');


var deviceFn = {
  saveUserLastTaggedLocation : function saveUserLastTaggedLocation (userId, locationId) {
    console.log('trying to save last loc');
      var q = Q.defer();

      User.update({
        _id: userId
      }, {
        $push : {
          lastTaggedLocation: {
            lid: locationId,
            dateTagged: Date.now()
          }
        }
      }, function (err, count) {
        console.log(arguments);
        if (err) {
          return q.reject(err);
        }

        if (count) {
          return q.resolve(true);
        }

        return q.reject(errors.nounce("UpdateFailed"));
      });
      return q.promise;
  },
  /**
   * adds a new location data to TC
   * @param {[type]} userId the author of the location/ first person to tag here
   * @param {[type]} body   Object with properties of the location.
   */
  addNewLocation : function addNewLocation (userId, body) {
      var q = Q.defer();
      var l = new TCLocation(body);
      l.coords =  [parseFloat(body.lon), parseFloat(body.lat)];
      l.longitude = body.lon;
      l.latitude = body.lat;
      l.author = userId;
      l.save(function (err, saveDoc) {
        if (err) {
          return q.reject(err);
        }

        if (saveDoc) {
          return q.resolve(saveDoc);
        }
      });
      return q.promise;
  },
  /**
   * reduced the mark / score of a location's verified counts by 1.
   * when a user tags an already existing location, this count is incremented.
   * if a user, does not agree with the data properties of that location, user can
   * discount this location entry. Location lookups are ordered by their verified counts
   * @param  {ObjectId} locationId Location ID
   * @return {[type]}            [description]
   */
  discountTaggedLocation: function discountTaggedLocation (locationId) {
      var q = Q.defer();

      TCLocation.update({
        _id: locationId
      }, {
        $inc: {
          verififiedCounts: -1
        }
      }, function (err, count) {
        if (err) {
          return q.reject(err);
        }

        if (count) {
          return q.resolve(true);
        }
      });
      return q.promise;
  },
    /**
   * increases the mark / score of a location's verified counts by 1.
   * when a user tags an already existing location, this count is incremented.
   * if a user, does not agree with the data properties of that location, user can
   * discount this location entry. Location lookups are ordered by their verified counts
   * @param  {ObjectId} locationId Location ID
   * @return {[type]}            [description]
   */
  countTaggedLocation: function countTaggedLocation (locationId) {
      var q = Q.defer();

      TCLocation.update({
        _id: locationId
      }, {
        $inc: {
          verififiedCounts: 1
        }
      }, function (err, count) {
        if (err) {
          return q.reject(err);
        }

        if (count) {
          return q.resolve(true);
        }
      });
      return q.promise;
  },
  /**
   * shows / looks up locations close to geoCoords
   * @param  {[type]} geoCords [description]
   * @param  {[type]} query    [description]
   * @return {[type]}          [description]
   */
  inProximity : function inProximity ( geoCoords, query) {
      // if (arguments.length == 1 && _.isArray(arguments[0])) {
      //   console.log('slice');
      //   geoCoords = arguments[0];
      //   query = arguments[1];
      // }


      var q = Q.defer(),
          limit = query.limit || 10,
          // maxDistance = query.maxDistance || 0.9;
          maxDistance = query.maxDistance || 0.055;
          maxDistance = maxDistance/111.12;

      if (geoCoords.length !== 2) {
        return q.reject(errors.nounce('InvalidParams'));
      }
      // TCLocation.find({
      //   "coords": {
      //     $near: coords,
      //     $maxDistance: 10
      //   }
      // })
      // TCLocation.find( {   coords: { "$near": [ parseFloat(geoCoords[0]), parseFloat(geoCoords[1])],  "$maxDistance": maxDistance }} )
      TCLocation.find( {
        coords: {
          "$near": geoCoords,
          "$maxDistance": maxDistance
        }
      })
      .limit(limit)
      .exec(function (err, locations) {
        if (err) {
          return q.reject(err);
        }

        if (locations.length) {
          return q.resolve(locations);
        }
        return q.resolve([]);
      });
      return q.promise;
  },
  findALocationById: function findALocationById (locationId) {
      var q = Q.defer();

      TCLocation.findOne({
        _id: locationId
      })
      .exec(function (err, l) {
        if (err) {
          return q.reject(err);
        }

        if (l) {
          return q.resolve(l);
        }
      });
      return q.promise;
  },
  /**
   * adds / creates a record of a check-in by a user
   * @param {[type]} params [description]
   */
  addACheckInRecord: function addACheckInRecord (params) {
      var q = Q.defer();
      var checkIndo = new CheckLog(params);

      checkIndo.save(function (err, saveDoc) {
        if (err) {
          return q.reject(err);
        }

        return q.resolve(saveDoc);
      });

      return q.promise;
  },
  /**
   * find a check-in record created by a specific user.
   * @param  {[type]} checkInId [description]
   * @param  {[type]} userId    [description]
   * @return {[type]}           [description]
   */
  findACheckInEntry: function findACheckInEntry (checkInId, userId) {
      var q = Q.defer();

      CheckLog.findOne({
        _id: checkInId,
        userId: userId
      })
      .exec(function (err, doc){
        if (err) {
          return q.reject(err);
        }
        if (!doc) {
          return q.reject(errors.nounce('DocumentNotFound'));
        }

        return q.resolve(doc);
      });

      return q.promise;
  },
  /**
   * updates an existing check-in entry. Expects a check-in
   * mongo objectId or a mongodb / mongoose document object.
   * This adds the checkout time for now.
   * @param  {[type]} mongoDoc [description]
   * @return {[type]}          [description]
   */
  updateACheckInEntry: function updateACheckInEntry (mongoDocOrId) {
      var q = Q.defer();

      if (_.isString(mongoDocOrId)) {
        CheckLog.update({
          _id: mongoDocOrId
        }, {
          $set: {
            checkOutTime: Date.now()
          }
        }, function (err, i) {
          if (err) {
            return q.reject(err);
          }
          if (!doc) {
            return q.reject(errors.nounce('UpdateFailed'));
          }

          return q.resolve(doc);
        });
      } else {
        mongoDocOrId.checkOutTime = Date.now();
        mongoDocOrId.save(function (err, doc) {
          if (err) {
            return q.reject(err);
          }
          if (!doc) {
            return q.reject(errors.nounce('UpdateFailed'));
          }

          return q.resolve(doc);
        });
      }

      return q.promise;
  }

};

/**
 * This provides functions and utilities for device and location specific operations.
 *
 */
function LocationDeviceObject () {

 }


 LocationDeviceObject.prototype.constructor = LocationDeviceObject;


/**
 * creates a GCM registeration Id entry for a device or refreshes the registration id.
 * @param  {[type]} regId the device registeration Id sent from Google Cloud Messaging.
 * @param  {[type]} deviceId  the uuid of the device
 * @param  {[type]} userId   the objectid of the currently logged in user.
 * @return {[type]}          [description]
 */
 LocationDeviceObject.prototype.registerOrUpdateGCMDevice = function registerOrUpdateGCMDevice (regId, deviceId, userId) {
  var q = Q.defer();
  var params = {
    active: true,
    userId: userId,
    deviceId: deviceId
  };


  Device.findOne(params)
  .exec(function (err, d) {
    if (err) {
      return q.reject(err);
    }

    //if found, update / refresh registration id
    if (d) {
      d.registerationId = regId;
      d.save(function (err, i) {
        if (err) {
          return q.reject(err);
        }
        return q.resolve(i);
      });
    }
    //if not found create a new entry
    else {
      var device = new Device(params);
      device.registerationId = regId;
      device.save(function (err, i ) {
        if (err) {
          return q.reject(err);
        }
        return q.resolve(i);
      });

    }

    return q.resolve(d);
  });

  return q.promise;
 };

/**
 * checks if a device has been registered to TagChief
 * @param  {[type]} deviceId [description]
 * @param  {[type]} userId   [description]
 * @return {[type]}          [description]
 */
 LocationDeviceObject.prototype.checkDeviceIdExist = function checkDeviceIdExist (regId, userId) {
  var q = Q.defer();
  console.log('check if device dey');
  Device.findOne({
    registerationId: regId,
    active: true,
    userId: userId
  })
  .exec(function (err, d) {
    if (err) {
      return q.reject(err);
    }
    return q.resolve(d);
  });

  return q.promise;
 };


/**
 * gets the registerationId for all the devices belonging to a user
 * @param  {[type]} userId [description]
 * @return {[type]}        [description]
 */
 LocationDeviceObject.prototype.getUserDevices = function getUserDevices (userId) {
  console.log('get user devices info');
  var q = Q.defer();
  Device.find({
    active: true,
    userId: userId
  }, 'registerationId')
  .exec(function (err, d) {
    if (err) {
      return q.reject(err);
    }
    return q.resolve(d);
  });

  return q.promise;
 };

/**
 * gets the registerationId for the user's currently active device. i.e.
 * the device sending the request right now.
 * @param  {[type]} userId [description]
 * @return {[type]}        [description]
 */
 LocationDeviceObject.prototype.getCurrentUserDevice = function getCurrentUserDevice (userId, deviceId) {
  var q = Q.defer();
  Device.find({
    active: true,
    userId: userId,
    deviceId: deviceId
  }, 'registerationId')
  .exec(function (err, d) {
    if (err) {
      return q.reject(err);
    }
    return q.resolve(d);
  });

  return q.promise;
 };

 LocationDeviceObject.prototype.registerDevice = function registerDevice (deviceId, userId, registerId) {
  var q = Q.defer(),
      device = new Device();
  device.deviceId = deviceId;
  device.userId = userId;
  device.registerationId = registerId;
  device.save(function (err, doc) {
    if (err) {
      return q.reject(err);
    }

    return q.resolve(doc);
  });

  return q.promise;
 };

  /**
   * check for all notification / messages belonging to a user and
   * sends the message via GCM or APN or both to all the devices
   * registered by the user.
   * @param  {ObjectId} userId     mongo user ObjectId
   * @return {Promise]}            Promise
   */
  LocationDeviceObject.prototype.sendUserNotices = function sendUserNotices (userId, mssgs) {
    var q = Q.defer();
    //find all user msgs
    //find user device / regIds
    //send mssgs to GSM
    var exampleMsgs = {

    };
    Device.find({
      userId: userId,
      active: true
    },'registerationId')
    .exec(function (err, devList) {
      if (err) {
        return q.reject(err);
      }

      if (devList.length) {
        return q.resolve(_.pluck(devList, 'registerationId'));
        // var regIds = _.pluck(devList, 'registerationId');
        // var gcm = new GPushMessenger(regIds);

        // gcm.sendMessage(regIds, function (re) {
        // });
      } else {
        return q.reject(new Error('no devices found'));
      }
      //send to gcm
    });

    return q.promise;
  };


  LocationDeviceObject.prototype.addTagLocation = function addTagLocation (body, userId) {
      var q = Q.defer(),
          locationData = body.location || {};
      if (locationData.id) {
        TCLocation.find({
          _id: locationData.id
        })
        .exec(function (err, d) {
          if (err) {
            return q.reject(err);
          } else {
            d.count = d.count + 1;
            d.save(function (err, i) {
              if (err) {
                return q.reject(err);
              }
              deviceFn.saveUserLastTaggedLocation(userId, body.location.id)
              .then(function () {
                return q.resolve(true);
              }, function (err) {
                return q.reject(err);
              });
            });
          }
        });
      } else {
        deviceFn.addNewLocation(userId, body)
        .then(function (locData) {
            deviceFn.saveUserLastTaggedLocation(locData.author, locData._id)
            .then(function () {
              return q.resolve(true);
            }, function (err) {
              return q.reject(err);
            });
        }, function (err) {
          return q.reject(err);
        });
      }
      return q.promise;
  };

  LocationDeviceObject.prototype.shareLocation = function shareLocation (userId, recpId, locationId) {
      var q = Q.defer();


      return q.promise;
  };

  LocationDeviceObject.prototype.whatsAroundMe = function (userId, coords, query, useInterest) {
      var q = Q.defer();

      //lookup user interest,
      //lookup near by locations
      //filter locations by user interest
      //send payload
      //list tagged locations in proximity...
      deviceFn.inProximity(coords, query)
      .then(function (locationLists) {
        if (_.isEmpty(locationLists)) {
          return q.resolve([]);
        } else {
          //should filter or sort by users interest first.
          return q.resolve(locationLists);
        }
      }, function (err) {
        return q.reject(err);
      });

      return q.promise;
  };

  LocationDeviceObject.prototype.getOneLocation = function getOneLocation (locationId) {
      var q = Q.defer();

      deviceFn.findALocationById(locationId)
      .then(function (l) {
        return q.resolve(l);
      }, function (err) {
        return q.reject(err);
      });

      return q.promise;
  };

  LocationDeviceObject.prototype.listLocationsByParams = function listLocationsByParams (userId, coords, params) {
    console.log('listing device by params');
      var q = Q.defer(), task = {};
      // switch (params.listType) {
      //   case 'checkin':
      //   task.task = deviceFn.findUserCheckedInLocations;
      //   task.args = [];
      //   break;
      //   case 'authored':
      //   task.task = deviceFn.findLocationsByMe;
      //   task.args = [];
      //   break;
      //   case 'countPlacesNearBy':
      //   task.task = deviceFn.inProximity;
      //   task.args = [coords, params];
      //   break;
      //   default:
      //   break;
      // }

      deviceFn.inProximity(coords, params)
      .then(function (d) {
        q.resolve(d);
      }, function (err) {
        q.reject(err);
      });

      return q.promise;
  };

  /**
   * Checks a user into a location
   * @param  {[type]} deviceId   [description]
   * @param  {[type]} locationId [description]
   * @param  {[type]} userId     [description]
   * @return {[type]}            [description]
   */
  LocationDeviceObject.prototype.checkIntoLocation = function checkIntoLocation (deviceId, locationId, userId) {
      var q = Q.defer();

      deviceFn.addACheckInRecord({
        deviceId: deviceId,
        locationId: locationId,
        userId: userId
      })
      .then(function (checkInId) {
        var pro = CheckLog.populate(checkInId, {
          path: 'locationId',
          select: 'name category dateAdded',
          model: 'Location'
        });
        pro.then(function (doc) {
          return q.resolve(doc);
        });
        //actions like reward a user / location should follow
      }, function (err) {
        return q.reject(err);
      })
      .catch(function (err) {
        console.log(err);
        return q.reject(err);
      });

      return q.promise;
  };

  /**
   * checks a user out of a location previously checked into.
   * @param  {[type]} checkInId  [description]
   * @param  {[type]} locationId [description]
   * @param  {[type]} userId     [description]
   * @return {[type]}            [description]
   */
  LocationDeviceObject.prototype.checkOutLocation = function checkOutLocation (checkInId, locationId, userId) {
      var q = Q.defer();

      deviceFn.findACheckInEntry(checkInId, userId)
      // .then(deviceFn.isValidCheckIn)
      .then(device.updateACheckInEntry)
      .then(function (response) {
        return q.resolve(response);
      }, function (err) {
        return q.reject(err);
      })
      .catch(function (err) {
        return q.reject(err);
      });
      return q.promise;
  };

  LocationDeviceObject.prototype.updateCheckInRecord = function updateCheckInRecord (body) {
    var q = Q.defer();

    FeedBackAnswers.update({
      checkInId: body.checkInId
    }, {
      $set: {
        questions: body.questions,
        nextQuestion: body.nextQuestion
      }
    }, {upsert: true}, function (err, doc) {
          if (err) {
            return q.reject(err);
          }
          if (!doc) {
            return q.reject(errors.nounce('UpdateFailed'));
          }

          return q.resolve(doc);
        });

    q.resolve(body);
    return q.promise;
  };

 module.exports = LocationDeviceObject;